﻿using UnityEngine;
using System.Collections;

public class VanishAfterTime : MonoBehaviour {
	public float seconds = 4f;
	public bool animate = true;
	
	// Update is called once per frame
	void Update () {
		if (animate && seconds < 1f){
			transform.localScale *= 0.95f;
		}
		
		seconds -= Time.deltaTime;
		if (seconds <= 0){
			Destroy (gameObject);
		}
	}
}