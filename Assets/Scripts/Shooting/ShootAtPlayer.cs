﻿using UnityEngine;
using System.Collections;

public class ShootAtPlayer : MonoBehaviour {

	public bool onlyWhenInSight = true;
	public Object bulletPrefab;
	public float range = 25;
	public float secondsBetweenBullets = 0.05f;
	
	float timer = 0;
	GameObject player;
	
	void Start () {
		bulletPrefab = bulletPrefab == null ? Resources.Load ("Prefabs/DontPlace/Bullet") : bulletPrefab;
		player = GameObject.Find ("Player");
	}
	
	void Update () {
		timer -= Time.deltaTime;
		if (timer <= 0f){
			timer = secondsBetweenBullets;
			
			if (player != null){
				RaycastHit hit;
				if (!onlyWhenInSight || Physics.Raycast(transform.position,player.transform.position-transform.position, out hit,range,~(1<<9))){
					if (!onlyWhenInSight || hit.collider.tag == "Player"){
						if (Vector3.Distance(transform.position,player.transform.position) < range){
							transform.LookAt(player.transform.position);
							GameObject kogel = Instantiate(bulletPrefab,transform.position+transform.forward +(new Vector3(Random.value,Random.value,Random.value)*0.5f),
							           transform.rotation) as GameObject;
							kogel.transform.LookAt(player.transform.position+new Vector3(Random.value*1.2f,Random.value*1.2f,Random.value*1.2f)-(Vector3.one*0.6f));
						}
					}
				}
			}
		}
	}
}