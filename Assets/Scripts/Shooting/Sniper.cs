﻿using UnityEngine;
using System.Collections;

public class Sniper : ShootWithMouse {
	public int ammo = 10;
	
	// Use this for initialization
	public override void Start () {
		bulletPrefab = Resources.Load ("Prefabs/DontPlace/SniperBullet");
	}
	
	// Update is called once per frame
	public override void Update () {
		if (ammo > 0 && Input.GetMouseButtonDown(0)){
			Shoot(0f);
			//ammo -= 1;
		}
	}
}
