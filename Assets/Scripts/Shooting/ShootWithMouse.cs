﻿using UnityEngine;
using System.Collections;

public class ShootWithMouse : MonoBehaviour {
	
	public int button = 0;
	public Object bulletPrefab;
	
	public float randomNess = 1.5f;
	
	// Use this for initialization
	public virtual void Start () {
		bulletPrefab = Resources.Load ("Prefabs/DontPlace/Bullet");
	}
	
	// Update is called once per frame
	public virtual void Update () {
		if (Input.GetMouseButton(button)){
			Shoot(randomNess);
		}
	}
	
	public virtual void Shoot(float rnd){
		GameObject obj = (Instantiate(bulletPrefab,transform.position+
		(new Vector3(Random.value,Random.value,Random.value)*rnd) - (Vector3.one*rnd*0.5f),
		 Camera.main.transform.rotation) as GameObject);
    }
}
