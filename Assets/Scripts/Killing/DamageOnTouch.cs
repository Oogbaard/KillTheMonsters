﻿using UnityEngine;
using System.Collections;

public class DamageOnTouch : MonoBehaviour {
	
	public int damage = 1;
	float age = 0f;
	
	public bool alsoDie = false;
	public bool onStay = false;
	public bool onlyDamagePlayer = false;
	// Update is called once per frame
	void Update () {
		age += Time.deltaTime;
	}
	
	void OnTriggerEnter(Collider col){
		TryToHit(col);
	}
	
	void OnTriggerStay(Collider col){
		if (onStay)
			TryToHit(col);
	}
	
	void OnCollisionEnter(Collision colli){
		TryToHit(colli.collider);
	}
	
	void OnCollisionStay(Collision colli){
		if (onStay)
			TryToHit(colli.collider);
	}
	
	void TryToHit(Collider col){
		if ((age > 0.035f)){
			if (col != null){
				Killable killable = col.GetComponent<Killable>();
				if (killable == null) killable = col.GetComponentInChildren<Killable>();
				if (killable == null) killable = col.GetComponentInParent<Killable>();
				
				if (killable != null){
					if (!onlyDamagePlayer || col.tag == "Player"){
						killable.Geraakt(damage);
					}
				}
				if (alsoDie){
					damage=0;
					Destroy (gameObject,0.01f);
				}
			}
		}
	}
}
