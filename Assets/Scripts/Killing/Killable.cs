﻿using UnityEngine;
using System.Collections;

public class Killable : MonoBehaviour {
	public int maxHitPoints = 1;
	public GameObject toDestroy;
	
	[HideInInspector]
	public int hitPoints;
	
	Object blokje;
	// Use this for initialization
	void Start () {
		blokje = Resources.Load ("Prefabs/DontPlace/Blokje");
		hitPoints = maxHitPoints;
	}
	
	public void Geraakt(int damage = 1){
		if (hitPoints > 0){
			hitPoints -= damage;
			if (hitPoints <= 0){
				
				for( int i=0;i<12;i++){
					Instantiate(blokje,
						transform.position+ (new Vector3(Random.value,Random.value,Random.value)-(Vector3.one*0.5f)),
						Quaternion.identity);
				}
				
				if (toDestroy != null)
					Destroy (toDestroy);
				else 
					Destroy (gameObject);
			}
		}
	}
}
