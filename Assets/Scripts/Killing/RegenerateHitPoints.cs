﻿using UnityEngine;
using System.Collections;

public class RegenerateHitPoints : MonoBehaviour {
	Killable kil;
	public float secondsBetween = 1f;
	
	float timer = 0f;
	void Start(){
		kil = GetComponent<Killable>();
	}
	
	void Update(){
		if (kil == null){enabled=false;return;}
		else if (kil.hitPoints < kil.maxHitPoints){
			timer += Time.deltaTime;
			if (timer >= secondsBetween){
				kil.hitPoints++;
				timer = 0f;
			}
		}
	}
}
