﻿using UnityEngine;
using System.Collections;

public class RotateTowardsPlayer : MonoBehaviour {
	
	public Transform targetTransform;
	
	public float speed = 0.4f;
	GameObject player;
	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
		if (targetTransform == null) targetTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		targetTransform.rotation = Quaternion.RotateTowards (targetTransform.rotation,
			Quaternion.LookRotation(player.transform.position-targetTransform.position),speed);
	}
}
