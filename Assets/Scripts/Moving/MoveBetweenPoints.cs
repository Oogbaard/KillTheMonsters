﻿using UnityEngine;
using System.Collections;

public class MoveBetweenPoints : MonoBehaviour {
	
	public float speed = 3f;
	
	public Transform wayPoint1;
	public Transform wayPoint2;
	
	bool heen = true;
	// Update is called once per frame
	void FixedUpdate () {
		if (wayPoint1 != null && wayPoint2 != null){
			if (heen){
				if (Vector3.Distance(transform.position,wayPoint1.position) < 0.2f){heen = false;}
				transform.position = Vector3.MoveTowards(transform.position,wayPoint1.position,speed*Time.deltaTime);
			}
			else {
				if (Vector3.Distance(transform.position,wayPoint2.position) < 0.2f){heen = true;}
				transform.position = Vector3.MoveTowards(transform.position,wayPoint2.position,speed*Time.deltaTime);
			}
		}
	}
}
