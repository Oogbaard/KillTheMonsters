﻿using UnityEngine;
using System.Collections;

public class Turn : MonoBehaviour {
	
	public Vector3 howMuch = Vector3.zero;
	public float speed = 1f;
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(howMuch*speed);
	}
}
