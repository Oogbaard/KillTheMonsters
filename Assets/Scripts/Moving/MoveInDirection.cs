﻿using UnityEngine;
using System.Collections;

public class MoveInDirection : MonoBehaviour {
	
	public bool relative = true;
	public Vector3 direction = Vector3.forward;
	public float speed = 5f;
	
	public Transform targetTransform;
	
	void Start(){
		if (targetTransform == null)
			targetTransform = transform;
	}
	
	void Update(){
		if (relative){
			targetTransform.position += transform.TransformVector(direction)*speed*Time.deltaTime;
		}
		else {
			targetTransform.position += direction*speed;
		}
	}
}
