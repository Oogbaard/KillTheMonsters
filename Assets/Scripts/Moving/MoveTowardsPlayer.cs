﻿using UnityEngine;
using System.Collections;

public class MoveTowardsPlayer : MonoBehaviour {
	
	public bool onlyWhenInSight = true;
	public float sightRange = 25f;
	public float speed = 1f;
	GameObject player;
	
	public Transform targetTransform;
	
	void Start(){
		if (targetTransform == null)
			targetTransform = transform;
			
		player = GameObject.Find ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (player != null){
			RaycastHit hit;
			if (!onlyWhenInSight || 
			Physics.Raycast(targetTransform.position,player.transform.position-targetTransform.position, out hit,sightRange)){
			
				if (!onlyWhenInSight || hit.collider.tag == "Player"){
					if (Vector3.Distance(targetTransform.position,player.transform.position) < sightRange){
						targetTransform.position += DirectionFromTo(targetTransform,player.transform) * speed * Time.deltaTime;
					}
				}
			}
		}
	}
	
	Vector3 DirectionFromTo(Transform t1, Transform t2){
		Vector3 d = t2.position - t1.position;
		d.Normalize();
		return d;
	}
}
