﻿using UnityEngine;
using System.Collections;

public class MoveUpAndDown : MonoBehaviour {
	public Vector3 howMuch = Vector3.up;
	public float speed = 1f;
	float t =0;
	
	// Update is called once per frame
	void Update () {
		t += Time.deltaTime;
		transform.position += howMuch*Mathf.Sin (t*speed)*0.05f;
	}
}
