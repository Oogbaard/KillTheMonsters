﻿using UnityEngine;
using System.Collections;

public class TurnUpAndDown : MonoBehaviour {
	public float degrees = 180f;
	public Vector3 howMuch = Vector3.up;
	public float speed = 1f;
	
	float g = 0f;
	
	// Update is called once per frame
	void Update () {
		if (g > degrees || g < 0f) {speed = -speed;}
		g += speed;
		
		transform.Rotate(howMuch * speed * Time.deltaTime);
	}
}
